import os
import cv2.cv2 as cv2
import time
# import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

IMG = os.path.join('resources', 'images', 'tea_object.jpg')
img = mpimg.imread(IMG)
copy = img.copy()

THRESHOLD = 4000

print('ok')
# Create SURF object
surf = cv2.xfeatures2d.SURF_create(THRESHOLD)
print('ok')
# starting a timer
start = time.time()

# Call the detect function of SIFT object to get keypoints
(kp, des) = surf.detectAndCompute(copy, None)

# elapsed
elapsed = time.time() - start

print('Time taken for detecting keypoints using SURF', elapsed, 'seconds')
print('Number of points', len(kp))

# draw the keypoints descriptors
cv2.drawKeypoints(copy, kp, copy, (255, 0, 0), flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
print('ok')

mpimg.imsave('./resources/test/sift_image_1.jpg', copy, format='JPG')

try:
    plt.figure('SURF features')

    plt.subplot(121)
    plt.title('Original')
    plt.xticks([]), plt.yticks([])
    plt.imshow(img)

    plt.subplot(122)
    plt.title('SURF Features')
    plt.xticks([]), plt.yticks([])
    plt.imshow(copy)

    plt.show()

except Exception:
    print('Error')
print('ok')
