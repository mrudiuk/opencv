import cv2.cv2 as cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


np.seterr(divide='ignore', invalid='ignore')

image = mpimg.imread('./files/lena.jpg')

try:
    img = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
except Exception:
    print('Doesn`t convert')
    img = image
    pass

Gx = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=5)
Gy = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5)

Gxy = np.sqrt(Gx * Gx + Gy * Gy)


try:
    theta = np.arctan(Gy/Gx)
except Exception:
    print('Error')
    pass

plt.figure('Sobel Filter')

plt.subplot(231)
plt.imshow(image, cmap='gray')
plt.title('Original')
plt.axis('off')

plt.subplot(232)
plt.imshow(Gx, cmap='gray')
plt.title('GX')
plt.axis('off')

plt.subplot(233)
plt.imshow(Gy, cmap='gray')
plt.title('Gy')
plt.axis('off')

plt.subplot(234)
plt.imshow(Gxy, cmap='gray')
plt.title('GXY')
plt.axis('off')

plt.subplot(235)
plt.imshow(theta, cmap='gray')
plt.title('Orientation')
plt.axis('off')

plt.tight_layout()

plt.show()

