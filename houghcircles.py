import cv2.cv2 as cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

image = mpimg.imread('./files/houghCircles.jpg')

gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
blurred = cv2.GaussianBlur(gray, (5, 5), 0)
circles = cv2.HoughCircles(blurred, cv2.HOUGH_GRADIENT, 1, 120, param1=50, param2=30, minRadius=50, maxRadius=90)

for i in circles[0, :]:
    cv2.circle(image, (i[0], i[1]), i[2], (0, 255, 0), 3)

    cv2.circle(image, (i[0], i[1]), 2, (0, 0, 255), 4)

surf = cv2.SURF(400)

plt.figure('Hough Circle Transform')
plt.gray()
plt.imshow(image)
plt.title('Circles')
plt.show()
