import cv2.cv2 as cv2
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def getSIFTfeatures(img):

	# Create SIFT object
	sift = cv2.xfeatures2d.SIFT_create()
	# starting a timer
	start = time.time()

	# Call the detect function of SIFT object to get keypoints
	kp = sift.detect(img, None)

	# elapsed
	elapsed =time.time() - start

	print('Time taken for detecting keypoints using SIFT', elapsed, 'seconds')
	print('Number of points', len(kp))

	return kp


def drawKeypoints(original ,img):
	# draw the keypoints descriptors
	cv2.drawKeypoints(mg, kp, img, (255, 0, 0), flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

	#mpimg.imsave('./resources/test/sift_image_1.jpg', copy, format='JPG')

	plt.figure('SIFT features')

	plt.subplot(121)
	plt.title('Original')
	plt.xticks([]), plt.yticks([])
	plt.imshow(origin)

	plt.subplot(122)
	plt.title('SIFT Features')
	plt.xticks([]), plt.yticks([])
	plt.imshow(img)

	plt.show()


