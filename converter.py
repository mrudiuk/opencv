import cv2.cv2 as cv2
from tkinter.filedialog import *


def open_file():
    root = Tk()
    img = askopenfilename()
    root.after(1000, root.destroy())
    root.mainloop()
    return img


try:
    image = cv2.imread(open_file())
    cv2.imwrite('./resources/images/test_11.jpg', image)
    print('DONE!')

except Exception:
    print('Ooops')
