import cv2.cv2 as cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def rotateImage(image, angle):
    center = tuple(np.array(image.shape[0:2])/2)
    rot_mat = cv2.getRotationMatrix2D(center, angle, 1.0)
    return cv2.warpAffine(image, rot_mat, image.shape[0:2],flags=cv2.INTER_LINEAR)


img = mpimg.imread('./files/lena.jpg')
rImage = rotateImage(img, 180)

plt.figure('Image Rotation')

plt.subplot(121)
plt.imshow(img)
plt.title('Original Image')
plt.axis('off')

plt.subplot(122)
plt.imshow(rImage)
plt.title('Rotated Image')
plt.axis('off')

plt.show()
