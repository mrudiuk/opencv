import cv2.cv2 as cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

affImg = mpimg.imread("./files/affine.jpg")
persImg = mpimg.imread("./files/sudoku_small.jpg")

rows1, cols1, ch1 = affImg.shape

apts1 = np.float32([[185, 814], [284, 493], [882, 690]])
apts2 = np.float32([[100, 787], [140, 455], [786, 551]])

M1 = cv2.getAffineTransform(apts1, apts2)
affTrans = cv2.warpAffine(affImg, M1, (cols1, rows1))

ppts1 = np.float32([[56, 65], [368, 52], [28, 387], [389, 390]])
ppts2 = np.float32([[0, 0], [300, 0], [0, 300], [300, 300]])

M2 = cv2.getPerspectiveTransform(ppts1, ppts2)
persTrans = cv2.warpPerspective(persImg, M2, (300, 300))


plt.figure("Affine and Perspective Transform")

plt.subplot(221)
plt.imshow(affImg)
plt.title("Original Image")
plt.axis("off")

plt.subplot(222)
plt.imshow(affTrans)
plt.title("Affine Transform")
plt.axis("off")

plt.subplot(223)
plt.imshow(persImg)
plt.title("Original Image")
plt.axis("off")

plt.subplot(224)
plt.imshow(persTrans)
plt.title("Perspective Transform")
plt.axis("off")

plt.tight_layout()

plt.show()


